{-# LANGUAGE DataKinds     #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE TypeOperators #-}

module PingAPI
  ( pingHandle
  , namedPingHandle
  , PingAPI
  ) where

import           Control.Monad.IO.Class
import           Data.Aeson
import           Data.Text              (Text)
import           GHC.Generics
import           Servant
import           System.Random

type PingAPI =
       "ping" :> Get '[JSON] Ping
  :<|> "ping" :> Capture "name" Name :> Get '[JSON] Ping

type Name = Text

data Ping = Ping
  { _name :: Maybe Name
  , _pong :: Int
  } deriving (Eq, Show, Generic)

instance FromJSON Ping
instance ToJSON Ping

genRandom :: IO Int
genRandom = randomRIO (1, 10)

pingHandle :: Handler Ping
pingHandle = liftIO $ Ping Nothing <$> genRandom

namedPingHandle :: Name -> Handler Ping
namedPingHandle n = liftIO $ (Ping $ Just n) <$> genRandom
