{-# LANGUAGE TypeOperators #-}

module Main where

import           Network.Wai.Handler.Warp
import           Servant

import           PingAPI                  (PingAPI, namedPingHandle, pingHandle)


server :: Server PingAPI
server = pingHandle
    :<|> namedPingHandle

pAPI :: Proxy PingAPI
pAPI = Proxy

-- 'serve' comes from servant and hands you a WAI Application,
-- which you can think of as an "abstract" web application,
-- not yet a webserver.
app :: Application
app = serve pAPI server

main :: IO ()
main = run 8080 app


