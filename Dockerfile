FROM haskell:8.2.2

WORKDIR /opt/app

# Need build-essentials for Stack :grimacing:
RUN apt update \
 && apt install -y build-essential netbase ca-certificates \
 && rm -rf /var/lib/apt/lists/*

RUN stack upgrade
RUN stack update

# Add just the .cabal file to capture dependencies
COPY ./package.yaml /opt/app/package.yaml
COPY ./stack.yaml /opt/app/stack.yaml

# Docker will cache this command as a layer, freeing us up to
# modify source code without re-installing dependencies
# (unless the .cabal file changes!)
RUN stack install --only-dependencies -j4

# Add and Install Application Code
COPY . /opt/app
RUN stack install

CMD ["api-exe"]
